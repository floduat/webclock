# webclock
[![](https://data.jsdelivr.com/v1/package/npm/clocks-for-websites/badge)](https://www.jsdelivr.com/package/npm/clocks-for-websites) ![NPM](https://img.shields.io/npm/l/clocks-for-websites) ![ETH](https://img.shields.io/badge/ETH-0xbec74686aebb51dfdc6b8045d4143ea63953bbd1-blue)<br>
JS clock for websites
# Features
Show time from the user's computer (So it will be the exact time of the user)
# Installation  
Paste the following code in your html file:
```html
<head>
<!-- your code-->
<script src="https://cdn.jsdelivr.net/npm/clocks-for-websites@1.0.3/clocks.min.js"></script>
<!-- your code-->
</head>
<body>
<!-- your code-->
<!-- For English language watches-->
<p id="clock"></p>
<!-- your code-->
</body>
```
### Live demo:   https://floduat.gitlab.io/webclock/ ###
### No more updates are planned ###
# Using
`<p id="clock"></p>`
